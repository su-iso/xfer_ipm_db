# get_last_ipm_run.py #

Current running crontab on area51
```
#!cron
* 0,2,4,6,8,10,12,14,16,18,20,22 * * * /usr/bin/python /home/xfer/git/xfer_ipm_db/get_last_ipm_run.py

```

This script gathers the last four ipm logs and overlays them to determine the final time bucket's worth of records.  These records are currently saved to 
```
#!path

/home/xfer/git/xfer_ipm_db/current_ipm_outfile
```
This file is overwritten each time the script runs.  Soon we will be picking up this file with  logstash.