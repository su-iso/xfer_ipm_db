#!/usr/bin/python

# import pdb
import sqlite3
import datetime
import sys
import sys
import os
import csv
import time


####
# Check for the output file

outfile = "/home/xfer/git/xfer_ipm_db/current_ipm_outfile"
now = datetime.date.today()
dbfile = '/home/xfer/git/xfer_ipm_db/ipm.db'


# pdb.set_trace()

# select the last three ipm logs based on time
p = "/hangar2-bay5/logs/network/arp/ipm/" + str(now.year) + "/" + now.strftime("%m") + "/"
os.chdir(p)

files = filter(os.path.isfile, os.listdir(p))
files = [os.path.join(p, f) for f in files]
files.sort(key=os.path.getmtime)
last_file_mtime = os.path.getmtime(files[-1])
now_time = time.time()

# Exit if existing output file less than 1 hour old, or if existing output file is newer than newest to-be-analyzed file.
try:
    os.path.exists(outfile)
    if (now_time - os.path.getmtime(outfile)) < 3600:
        sys.exit()
    if os.path.getmtime(outfile) < os.path.getmtime(last_file_mtime):
        sys.exit()
except:pass

# Truncate outfile
open(outfile,'w').close()

# Get the list of the four newest files
three_of_four_last = files[-4:-1]
# Get the name of the newest file
last_file = files[-1]

####
# datetime.datetime(2017, 6, 22, 9, 30, 35, 951771)
# CREATE TABLE IF NOT EXISTS ipm_lines (ip,type,netdb,hwaddr,nictype,first,last,times, PRIMARY KEY (ip,hwaddr,times));
# CREATE TABLE IF NOT EXISTS new_ipm_lines (ip,type,netdb,hwaddr,nictype,first,last,times, PRIMARY KEY (ip,hwaddr,times));
# CREATE TABLE IF NOT EXISTS current_state (last_update_time,last_file, PRIMARY KEY (ip,hwaddr,times));

# truncate db
open(dbfile,'w').close()

# conn = sqlite3.connect('/home/xfer/git/xfer_ipm_db/ipm.db')
conn = sqlite3.connect(dbfile)
c = conn.cursor()

c.execute("""DROP TABLE if exists ipm_lines;""")
c.execute("""DROP TABLE if exists new_ipm_lines;""")
conn.commit()
last_three_table = """CREATE TABLE IF NOT EXISTS ipm_lines (ip,type,netdb,hwaddr,nictype,first,last,times, PRIMARY KEY (ip,hwaddr,times));"""
current_records_table = """CREATE TABLE IF NOT EXISTS new_ipm_lines (ip,type,netdb,hwaddr,nictype,first,last,times, PRIMARY KEY (ip,hwaddr,times));"""
c.execute(last_three_table)
c.execute(current_records_table)


# c.execute("""BEGIN TRANSACTION""")
result = []
for filename in three_of_four_last:
    with open(filename, "r+") as fp:
        for i in fp.readlines():
            tmp = i.split()
            try:
                if "match" not in tmp[7]:
                    result.append((tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6],tmp[7]))
            except:pass
# print result

c.executemany('INSERT OR IGNORE INTO ipm_lines VALUES (?,?,?,?,?,?,?,?)', result )

result = []
with open(last_file, "r+") as fp:
    for i in fp.readlines():
        tmp = i.split()
        try:
            if "match" not in tmp[7]:
                result.append((tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6],tmp[7]))
        except:pass
# print result

c.executemany('INSERT OR IGNORE INTO new_ipm_lines VALUES (?,?,?,?,?,?,?,?)', result )
# c.execute("""COMMIT""")


conn.commit()
conn.close()

time.sleep(10)

newconn = sqlite3.connect(dbfile)
nc = newconn.cursor()

nc.execute("SELECT * from new_ipm_lines where not exists (select 1 from ipm_lines where hwaddr=new_ipm_lines.hwaddr and ip=new_ipm_lines.ip and times=new_ipm_lines.times)");
all_rows = nc.fetchall()

with open (outfile, 'w+') as f:
    writer = csv.writer(f)
    writer.writerows(all_rows)
    # for row in data:
    #    writer.writerows(row)
conn.close()
